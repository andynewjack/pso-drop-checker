// #includes
#include <iostream>
#include <string>
#include <iomanip> // For formatting hex nicely during testing.
#include <fstream> // For file i/o. Mainly to read items.txt
#include <map> // For making list to hold items.txt
#include <Windows.h> // For ReadProcessMemory()
#include <TlHelp32.h> // For CreateToolhelp32Snapshot(). Used for finding PID of PSO.
#include "PSODropCheck.h" // For specials & techs array when they get implemented, plus other useful things.

using namespace std;

// Function prototypes
int GetPsoPid() {
   int pid = -1;
   int userpid = -1;
   int i = 0;
   int multiclient[8] = { NULL }; // Assuming someone doesn't have more than 8 pso clients open at once....
   int multiclientindex = 0;
   PROCESSENTRY32 process = { 0 };
   process.dwSize = sizeof(PROCESSENTRY32); // Known bug apparently requires the size to be declared, since the automatic size detection that should happen messes up somewhere. Need to look into this more.

   // Take snapshot of all currently running processes. Walk through them until we find PSO.
   HANDLE snapshot = CreateToolhelp32Snapshot( TH32CS_SNAPPROCESS, 0 );

   if ( Process32First(snapshot, &process) ) {// Assuming CreateTool succeeded.
      do {
         // szExeFile member of PROCESSENTRY32 struct is the name of the process. Need to check this for "psobb.exe"
         // Return the PID if we find it. Otherwise go to next process.
         // fix typecast later. There must be an actual compare function for whatever datatype szExeFile is.
         for (i = 0; i < strlen(process.szExeFile); i++) {
            process.szExeFile[i] = tolower(process.szExeFile[i]);
         }
         /*cout << process.szExeFile << endl;*/
         if ( (string) process.szExeFile == "psobb.exe") {
            //pid = process.th32ProcessID;
            multiclient[multiclientindex++] = process.th32ProcessID; // Save the pid of all the running pso clients and let the user choose which one to hook into if multiple clients are running. This may or may not be updated later to automatically create a child process for each client, assuming the user wants a drop checker for all clients?
            //return pid;
         }
      } while ( Process32Next(snapshot, &process) );
      if (multiclientindex > 1) {
         cout << "Multiple PSO clients found! Which one would you like to use?" << endl;
         for (i = 0; i < multiclientindex; i++) {
            cout << i << ") " << multiclient[i] << endl;
         }
         while (true) {
            cin >> userpid;
            if (userpid >= 0 && userpid < multiclientindex) {
               pid = multiclient[userpid];
               break;
            }
            else {
               cout << "Not a valid choice. Choose again, or CTRL + c to quit." << endl;
               cin.clear();
               cin.ignore(INT_MAX, '\n');
            }
         }
      }
      else if (multiclientindex == 1) {
         pid = multiclient[0];
      }
      else {
         // If we get here, it means pso was not found and will return -1. Who doesn't play PSO ? ? ?
         std::cout << "Why are you not playing PSO?" << endl;
         return -1;
      }
   }
   else { // CreateTool failed.
      std::cout << "couldn't create snapshot of processes" << endl;
      std::cout << "GetLastError(): " << GetLastError() << endl;
   }
   return pid;
}

HANDLE GetConsoleHandle() {
   return GetStdHandle(STD_OUTPUT_HANDLE);
}

// Reads the address value located at magic offset, swaps the endian, adds 16 and returns the value.
// The returned value is the actual address of the drop table. PS: Rust is dumb.
unsigned int GetOffset(HANDLE psoHandle, void *magicPtr) {
   BYTE rpmBuffer[4] = {};
   unsigned int dropTable = 0;

   ReadProcessMemory(psoHandle, magicPtr, &rpmBuffer, sizeof(rpmBuffer), NULL); // Read 4 bytes at the magic offset.
   dropTable = (rpmBuffer[3] << 24) | (rpmBuffer[2] << 16) | (rpmBuffer[1] << 8) | (rpmBuffer[0]); // swap endian
   dropTable += 16;
   return dropTable;
}

int GetDrops(HANDLE psoHandle, HANDLE hConsole, unsigned int dropTable, std::map<unsigned int, string> &itemsMap) {
   BYTE rpmBuffer[DROPSTEP] = {};
   int area;
   int item;
   int i, j;
   unsigned int offset = 0;
   unsigned int itemCode = 0;
   unsigned int numItems = 0;
   unsigned int dropNum = 0;
   Weapon weapon;
   Armour armour;
   Mag mag;
   Tool tool;
   static std::map<unsigned int, unsigned int> oldDrops;
   static std::map<unsigned int, unsigned int> newDrops;
   std::map<unsigned int, unsigned int>::iterator it;
   ofstream outputfile;
   static int z = 0;


   outputfile.open("drops.log", ios::out | ios::app);

   for (area = 0; area < AREACOUNT; area++) {
      for (item = 0; item < MAXITEMS; item++) {
         offset = dropTable + (AREASTEP * area) + (DROPSTEP * item);
         ReadProcessMemory(psoHandle, (void*)offset, &rpmBuffer, sizeof(rpmBuffer), NULL);
         itemCode = (rpmBuffer[0] << 16) | (rpmBuffer[1] << 8) | (rpmBuffer[2]);
         // Only print out the address content if there's actually an item there.
         if (itemCode) {
            numItems++;
            dropNum = (rpmBuffer[12] << 24) | (rpmBuffer[13] << 16) | (rpmBuffer[14] << 8) | (rpmBuffer[15]);
            newDrops.insert(pair<unsigned int, unsigned int>(dropNum, itemCode));

            it = oldDrops.find(dropNum); // Check if item was already printed.
            if (it == oldDrops.end()) { // Item was not printed previously.
               if (DEBUG) {
                  // debugging prints
                  std::cout << "offset " << setfill('0') << setw(8) << hex << offset << ": ";
                  outputfile << "offset " << setfill('0') << setw(8) << hex << offset << ": ";
                  for (i = 0; i < 9; i++) {
                     for (j = 0; j < 4; j++) {
                        std::cout << setfill('0') << setw(2) << hex << uppercase << (rpmBuffer[(4 * i) + j] & 0xFF);
                        outputfile << setfill('0') << setw(2) << hex << uppercase << (rpmBuffer[(4 * i) + j] & 0xFF);
                     }
                     std::cout << " ";
                     outputfile << " ";
                  } // endfor
                  std::cout << endl;
                  outputfile << endl;
               }

               // switch to call right parse function
               switch (rpmBuffer[0]) {
               case 0: // regular weapon or srank
                  weapon.parse(rpmBuffer, itemCode, itemsMap);
                  weapon.printWep(hConsole, outputfile);
                  weapon.reset();
                  break;

               case 1: // Armour/Shield/Unit
                  armour.parse(rpmBuffer, itemCode, itemsMap);
                  armour.print(hConsole, outputfile);
                  armour.reset();
                  break;

               case 2: // Mag
                  mag.parse(rpmBuffer, itemCode, itemsMap);
                  mag.print(hConsole, outputfile);
                  mag.reset();
                  break;

               case 3: // Tool
                  tool.parse(rpmBuffer, itemCode, itemsMap);
                  tool.print(hConsole, outputfile);
                  tool.reset();
                  break;

               case 4: // meseta
                  //std::cout << "Meseta" << endl; // Actual amount is in bytes 16-19 in little endian
                  break;

               default:
                  return 1; // Leave the entire function and return 1 to indicate that garbage values have been read. This will cause the main function to find the new offset for the drop table. This repeats until person logs in again.
               } //end switch
            } //endif(it == oldDrops) iterator
         } // endif(itemCode)
      } // end for(item)
   } // end for(area)
   oldDrops.clear();
   oldDrops.swap(newDrops);
   outputfile.close();
   return 0;
}

map<unsigned int, string> CreateItemsMap() {
   // items.txt path will have to be changed once the program runs directly without visual studio.
   //ifstream infile("..\\..\\references\\psodropcheck-master\\items.txt");
   ifstream infile("items.txt");
   string name = "";
   char whitespace;
   int value = 0;
   map<unsigned int, string> itemsMap; // Create a map to store all the items

   // Check if we opened the file.
   if (infile.is_open()) {
      // Loop as long as there is still data to read.
      while (!infile.eof()) {
         infile >> hex >> value;
         infile.get(whitespace); // skip the white space between the code and name
         getline(infile, name);
         itemsMap.insert( pair<unsigned int, string>(value, name) );
      }
      infile.close();
   }
   else {
      std::cout << "items.txt could not be opened" << endl;
   }
   return itemsMap;
}


/* 
 * Gonna need a search method at some point to search through entire items list.
 * Binary search initally until a better method can be implemented?
 * Should have code laying around somewhere. Maybe from RTOS?
 * Gonna need to do some design magic to see if items have any patterns.
 */

// Main code
// Should make SUPER SMALL COMPACT main code at some point. Modularize everything.
int main( int argc, char *argv[] ) {
   std::cout << "Andy's nifty PSO floor scanner! Ported from Jake's lame-ass Rust version." << endl;
   std::cout << "This is still a work in progress. Items may still be missing from the list." << endl;
   std::cout << "You must start PSO first before starting this program." << endl;
   std::cout << "Logging out to the main screen should not be broken anymore..." << endl;
   std::cout << "Questions/comments/concerns/ can be sent to me through forums or Discord." << endl << endl;
   HANDLE psoHandle = NULL;
   HANDLE hConsole = NULL;
   map<unsigned int, string> itemsMap;
   BYTE rpmBuffer[16] = {}; // Buffer for ReadProcessMemory. Items are 16 bytes long. (0x00000000 x 4)
   char input = NULL;
   unsigned int dropTable = 0x00; // Actual mem location of drops
   int psoPid = -1;
   int i = 0;
   int loggedOut = 0;

   psoPid = GetPsoPid();

   // Check if we found pid.
   if ( psoPid != -1 ) {
      std::cout << "Found PSO with PID: " << psoPid << endl;

      // Have PID. Can call OpenProcess now to get a window Handle for ReadProcessMemory
      psoHandle = OpenProcess( PROCESS_VM_READ, FALSE, psoPid );

      if ( psoHandle != NULL ) { // Successfully got a Handle to PSO.
         std::cout << "Found handle" << endl;

         hConsole = GetConsoleHandle();

         // get the actual offset for the droptable from the magicoffset
         dropTable = GetOffset(psoHandle, (void*)MAGICDROPOFFSETPOINTER);
         itemsMap = CreateItemsMap();
         std::cout << "Running!" << endl;
         while (1) {
            loggedOut = GetDrops(psoHandle, hConsole, dropTable, itemsMap);
            Sleep(1000);
            if (loggedOut) {
               dropTable = GetOffset(psoHandle, (void*)MAGICDROPOFFSETPOINTER);
            }
         }
      }
      else {
         /* 
          * Did not get Handle for PSO.
          * Exit for now.
          * Not sure when this would happen ie: Pso is found but cannot get Handle. Might be permission issue?
          * Have to check docs again for error codes.
          */
         cout << "Couldn't get a Handle for PSO. Exiting now." << endl;
         std::cin.ignore();
         exit(1);
      }
   }
   // Couldn't find pid for a running pso process.
   else {
      // Exit program for now.
      // Add code later to keep searching for pso.
      std::cout << "Couldn't find pso running" << endl;
      std::cin.ignore();
      exit(1);
   }

   std::cout << "Exiting" << endl;
   std::cin.ignore();
   return 0;
}