#ifndef PSODROPCHECK_H
#define PSODROPCHECK_H

#include <string>
#include <Windows.h>
#include <map>
#include <fstream>

/*
 * Will probably put specials.txt and techs.txt in here as 2 arrays later on at some point.
 * Also item structs will go in here eventually.
 */

// Magic numbers thanks to Jake from itemdrop.rs
#define MAGICDROPVALUE         {0xE6, 0x01, 0x00, 0x55, 0x53, 0x45, 0x00}
#define MAGICDROPOFFSET        24
#define MAGICDROPOFFSETPOINTER 0x00A8D8A4
#define DROPSTEP               0x24
#define AREASTEP               0x1B00
#define AREACOUNT              18
#define MINSTARTADDR           0x00
#define MAXITEMS               150
#define ITEMSIZE               0x24
#define DEBUG                  0x0

const std::string techs[] = {
   "Foie",     // 0x00
   "Gifoie",   // 0X01
   "Rafoie",   // 0X02
   "Barta",    // 0X03
   "Gibarta",  // 0X04
   "Rabarta",  // 0X05
   "Zonde",    // 0X06
   "Gizonde",  // 0X07
   "Razonde",  // 0X08
   "Grants",   // 0X09
   "Deband",   // 0X0A
   "Jellen",   // 0X0B
   "Zalure",   // 0X0C
   "Shifta",   // 0X0D
   "Ryuker",   // 0X0E
   "Resta",    // 0X0F
   "Anti",     // 0X10
   "Reverser", // 0X11
   "Megid"     // 0x12
};

const std::string specials[] = {
   "",          // 0x00
   "Draw ",     // 0x01
   "Drain ",    // 0x02
   "Fill ",     // 0x03
   "Gush ",     // 0x04
   "Heart ",    // 0x05
   "Mind ",     // 0x06
   "Soul ",     // 0x07
   "Geist ",    // 0x08
   "Master's ", // 0x09
   "Lord's ",   // 0x0A
   "King's ",   // 0x0B
   "Charge ",   // 0x0C
   "Spirit ",   // 0x0D
   "Berserk ",  // 0x0E
   "Ice ",      // 0x0F
   "Frost ",    // 0x10
   "Freeze ",   // 0x11
   "Blizzard ", // 0x12
   "Bind ",     // 0x13
   "Hold ",     // 0x14
   "Seize ",    // 0x15
   "Arrest ",   // 0x16
   "Heat ",     // 0x17
   "Fire ",     // 0x18
   "Flame ",    // 0x19
   "Burning ",  // 0x1A
   "Shock ",    // 0x1B
   "Thunder ",  // 0x1C
   "Storm ",    // 0x1D
   "Tempest ",  // 0x1E
   "Dim ",      // 0x1F
   "Shadow ",   // 0x20
   "Dark ",     // 0x21
   "Hell ",     // 0x22
   "Panic ",    // 0x23
   "Riot ",     // 0x24
   "Havoc ",    // 0x25
   "Chaos ",    // 0x26
   "Devil's ",  // 0x27
   "Demon's "   // 0x28
};

// Might try std::map for these.
// Could also waste space with extra specials for padding to make the indecies match /shrug
const std::string srankSpecials[] {
   "",         // No special
   "Jellen ",   // (Weakens Enemy Attack)    01
   "Zalure ",   // (Weakens Enemy Defense    02
   "Burning ",  // (Fire Attribute)          05
   "Tempest ",  // (Thunder Attribute)       06
   "Blizzard ", // (Ice Attribute)           07
   "Arrest ",   // (Paralysis)               08
   "Chaos ",    // (Confusion)               09
   "Kings ",    // (Drains Enemy XP)         10
   "Hell ",     // (1 Hit Kill)              0A
   "Spirit ",   // (Drains 10 % of Total TP for Double Damage)   0B
   "Berserk ",  // (Drains 10 % of Total HP for Double Damage)   0C
   "Demon's ",  // (Reduces HP to 1/4))      0D
   "Gush ",     // (Steals Enemy HP)         0E
   "Geist "     // (Steals Enemy TP)         0F
};

const std::string magColour[] {
   "Red",
   "Blue",
   "Yellow",
   "Green",
   "Purple",
   "Black",
   "White",
   "Cyan",
   "Brown",
   "Orange",
   "Slate Blue",
   "Olive",
   "Turquoise",
   "Fuschia",
   "Grey",
   "Cream",
   "Pink",
   "Dark Green"
};

const std::string magPB[] {
   "Farlla",
   "Estlla",
   "Golla",
   "Pilla",
   "Leilla",
   "Mylla & Youlla"
};


class Weapon {
public:
   // Constructors probably aren't needed. Better to initialize everything anyway just in case?
   Weapon();
   std::string special;
   std::string name;
   char grind;
   char native;
   char abeast;
   char machine;
   char dark;
   char hit;
   void parse(BYTE(&rpmBuffer)[ITEMSIZE], unsigned int itemCode, std::map<unsigned int, std::string> &itemsMap);
   void reset();
   void printWep(HANDLE hConsole, std::ofstream& outputfile);
   void printSrank(HANDLE hConsole, std::ofstream& outputfile);
};



class Armour {
public:
   Armour();
   std::string name;
   BYTE slots;
   unsigned short type;
   unsigned short dfp;
   unsigned short evp;
   void parse(BYTE (&rpmBuffer)[ITEMSIZE], unsigned int itemCode, std::map<unsigned int, std::string> &itemsMap);
   void reset();
   void print(HANDLE hConsole, std::ofstream& outputfile);
};



class Mag {
public:
   Mag ();
   std::string name;

   void parse(BYTE(&rpmBuffer)[ITEMSIZE], unsigned int itemCode, std::map<unsigned int, std::string> &itemsMap);
   void reset();
   void print(HANDLE hConsole, std::ofstream& outputfile);
};

class Tool
{
public:
   Tool();
   std::string name;
   short level;
   short quantity;
   bool isTech;
   void parse(BYTE(&rpmBuffer)[ITEMSIZE], unsigned int itemCode, std::map<unsigned int, std::string> &itemsMap);
   void reset();
   void print(HANDLE hConsole, std::ofstream& outputfile);
};

#endif