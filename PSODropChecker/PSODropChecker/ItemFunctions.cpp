#include "PSODropCheck.h"
#include <iostream>
#include <iomanip> // For formatting hex nicely.

using namespace std;

COORD cursorPos;
CONSOLE_SCREEN_BUFFER_INFO csbiInfo;

// Member functions
Weapon::Weapon() {
   special = "";
   name = "";
   grind = 0;
   native = 0;
   abeast = 0;
   machine = 0;
   dark = 0;
   hit = 0;
}

// Parse weapon. Still need parse to srank.
void Weapon::parse(BYTE(&rpmBuffer)[ITEMSIZE], unsigned int itemCode, std::map<unsigned int, string> &itemsMap) {
   //    * VVVVVVGG,  SS00 PPPP, PPPP PPPP, 0000 0000
   //      [0]                 ...                 [15]
   special = specials[rpmBuffer[4] & 0x7F];
   name = itemsMap[itemCode];
   grind = rpmBuffer[3];

   if (rpmBuffer[6] != 0x00) {
      switch (rpmBuffer[6]) {
      case 0:
         break;
      case 1:
         native = rpmBuffer[7];
         break;
      case 2:
         abeast = rpmBuffer[7];
         break;
      case 3:
         machine = rpmBuffer[7];
         break;
      case 4:
         dark = rpmBuffer[7];
         break;
      case 5:
         hit = rpmBuffer[7];
         break;
      default:
         break;
      }
   }

   if (rpmBuffer[8] != 0x00) {
      switch (rpmBuffer[8]) {
      case 0:
         break;
      case 1:
         native = rpmBuffer[9];
         break;
      case 2:
         abeast = rpmBuffer[9];
         break;
      case 3:
         machine = rpmBuffer[9];
         break;
      case 4:
         dark = rpmBuffer[9];
         break;
      case 5:
         hit = rpmBuffer[9];
         break;
      default:
         break;
      }
   }

   if (rpmBuffer[10] != 0x00) {
      switch (rpmBuffer[10]) {
      case 0:
         break;
      case 1:
         native = rpmBuffer[11];
         break;
      case 2:
         abeast = rpmBuffer[11];
         break;
      case 3:
         machine = rpmBuffer[11];
         break;
      case 4:
         dark = rpmBuffer[11];
         break;
      case 5:
         hit = rpmBuffer[11];
         break;
      default:
         break;
      }
   }
}

void Weapon::reset() {
   special = "";
   name = "";
   grind = 0;
   native = 0;
   abeast = 0;
   machine = 0;
   dark = 0;
   hit = 0;
}

void Weapon::printWep(HANDLE hConsole, std::ofstream& outputfile) {
   //outputfile.open("drops.txt");
   std::cout << special << name << "+" << dec << (short)grind;
   outputfile << special << name << "+" << dec << (short)grind;
   GetConsoleScreenBufferInfo(hConsole, &csbiInfo);
   csbiInfo.dwCursorPosition.X = 24;
   SetConsoleCursorPosition(hConsole, csbiInfo.dwCursorPosition);
   std::cout << "[" << (short)native << "/" << (short)abeast << "/" << (short)machine << "/" << (short)dark << "\|" << (short)hit << "]" << endl;
   outputfile << " [" << (short)native << "/" << (short)abeast << "/" << (short)machine << "/" << (short)dark << "\|" << (short)hit << "]" << endl;
   //outputfile.close();
}

void Weapon::printSrank(HANDLE hConsole, std::ofstream& outputfile) {
   //outputfile.open("drops.txt");
   std::cout << "Srank " << name << endl;
   outputfile << "Srank " << name << endl;
   //outputfile.close();
}

Armour::Armour() {
   name = "";
   slots = 0;
   dfp = 0;
   evp = 0;
   type = 0;
}

void Armour::parse(BYTE(&rpmBuffer)[ITEMSIZE], unsigned int itemCode, std::map<unsigned int, string> &itemsMap) {
   //    * VVVVVV00, 000SDDDD, 00EEEE00, 00000000
   //   /*
   //    * Unit structure:
   //    * VVVVVV00,000000SS,SS000000,00000000
   //    * V = Unit ID in HEX
   //    * S = Suffix in HEX. (0100 for +, 0300 for ++, FFFF for -, FEFF for --)
   //    */
   name = itemsMap[itemCode];
   type = (itemCode & 0x000F00) >> 8;
   if (type == 0x02) { // Shield 0x0102yy
      dfp = (rpmBuffer[7] << 8) | (rpmBuffer[6]);
      evp = (rpmBuffer[10] << 8) | (rpmBuffer[9]);
   }
   else if(type == 0x01) { // Armour 0x0101yy
      slots = rpmBuffer[5];
      dfp = (rpmBuffer[7] << 8) | (rpmBuffer[6]);
      evp = (rpmBuffer[10] << 8) | (rpmBuffer[9]);
   }
}

void Armour::reset() {
   name = "";
   slots = 0;
   type = 0;
   dfp = 0;
   evp = 0;
}

void Armour::print(HANDLE hConsole, std::ofstream& outputfile) {
   //outputfile.open("drops.txt");
   if (type == 0x01) { // armour
      std::cout << dec << (short)slots << "s " << name;
      outputfile << dec << (short)slots << "s " << name;
      GetConsoleScreenBufferInfo(hConsole, &csbiInfo);
      csbiInfo.dwCursorPosition.X = 24;
      SetConsoleCursorPosition(hConsole, csbiInfo.dwCursorPosition);
      std::cout << (short)dfp << " DFP/" << (short)evp << " EVP" << endl;
      outputfile << " " << (short)dfp << " DFP/" << (short)evp << " EVP" << endl;
   }
   else if (type == 0x02) { // shield
      std::cout << dec << name;
      outputfile << dec << name;
      GetConsoleScreenBufferInfo(hConsole, &csbiInfo);
      csbiInfo.dwCursorPosition.X = 24;
      SetConsoleCursorPosition(hConsole, csbiInfo.dwCursorPosition);
      std::cout << (short)dfp << " DFP/" << (short)evp << " EVP" << endl;
      outputfile << " " << (short)dfp << " DFP/" << (short)evp << " EVP" << endl;
   }
   else {
      std::cout << name << endl;
      outputfile << name << endl;
   }
   //outputfile.close();
}

Mag::Mag() {
   name = "";
}

void Mag::parse(BYTE(&rpmBuffer)[ITEMSIZE], unsigned int itemCode, std::map<unsigned int, string> &itemsMap) {
   std::cout << "Itemcode: " << itemCode << endl;
   printf("Itemcode: %.6X\n", (itemCode & 0xFFFF00));
   name = itemsMap[(itemCode&0xFFFF00)];
}

void Mag::reset() {
   name = "";
}

void Mag::print(HANDLE hConsole, std::ofstream& outputfile) {
   //outputfile.open("drops.txt");
   std::cout << name << endl;
   outputfile << name << endl;
   //outputfile.close();
}

Tool::Tool() {
   name = "";
   level = 0;
   quantity = 0;
}

void Tool::parse(BYTE(&rpmBuffer)[ITEMSIZE], unsigned int itemCode, std::map<unsigned int, string> &itemsMap) {
   //    * VVVVVV00, 00AA0000, 00000000, 00000000
   //    * Tech disk structure:
   //    * 0302LL00, VV000000, 00000000, 00000000
   //    * V = Value of Technique
   //    * L = Level of Technique in HEX
   //    */
   name = itemsMap[itemCode];
   if ((itemCode & 0x00FF00) == 0x000200) {
      level = (itemCode & 0xFF) + 1;
      name = techs[rpmBuffer[4]];
   }

   quantity = rpmBuffer[5];
}

void Tool::reset() {
   name = "";
   quantity = 0;
   level = 0;
}

void Tool::print(HANDLE hConsole, std::ofstream& outputfile) {
   //outputfile.open("drops.txt");
   if (level == 0) { // Not a tech disk
      std::cout << name;
      outputfile << name;
      if (quantity > 0) {
         std::cout << " x" << dec << quantity;
         outputfile << " x" << dec << quantity;
      }
      std::cout << endl;
      outputfile << endl;
   }
   else { // Tech disk
      std::cout << name << " Lv." << dec << level << endl;
      outputfile << name << " Lv." << dec << level << endl;
   }
   //outputfile.close();
}